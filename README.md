**Description**

Converts an .xlsx document into CSV.

The script is primarily useful for german Excel users who need to convert
their files into a propper ',' seperated CSV. A german Excel is exporting
CSV by default as ';' seperated file.

**Setup**

Recommendation: Use a virtual environment like [Virtualenv](https://virtualenv.pypa.io/en/latest/) before installing pip
modules.

```
pip install -r requirements.txt
```

**Usage**

Simple import/export. The outputfile has the same prefix as
the import with '.csv' as extension.

```
xlsx2csv.py -f filename.xlsx
```

Result: filename.csv


Usage with custom outputfile.

```
xlsx2csv.py -f filename.xlsx -o mycsv.csv
```

Result: mycsv.csv