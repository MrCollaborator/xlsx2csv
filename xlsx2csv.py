"""Converts an .xlsx document into CSV.

The script is primarily useful for german Excel users who need to convert
their files into a propper ',' seperated CSV. A german Excel is exporting
CSV by default as ';' seperated file.

Usage:..

    pip install -r requirements.txt



"""
from io import TextIOWrapper
import argparse
import pandas as pd


def check_file(file: TextIOWrapper, ext: str = "xlsx") -> None:
    if not file:
        print(
            "Input file needed. Use 'python xlsx2csv.py --help' "
            "for more information."
        )
        exit()

    if file.name[-(len(ext) + 1) :] != "." + ext:
        print(f"Input File needs to have '.{ext}' extension.")
        exit()


def xlsx2csv(inputfile: str, outputfile: str = None) -> None:
    xlsx = pd.read_excel(inputfile)

    if not outputfile:
        outputfile = inputfile.split(".")[0] + ".csv"

    xlsx.to_csv(outputfile)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Convert xlsx to csv.")
    parser.add_argument(
        "inputfile",
        nargs="?",
        help="Name of the input file.",
        type=argparse.FileType("r"),
    )
    parser.add_argument(
        "-o", nargs="?", help="Name of the output file. ", type=argparse.FileType("w")
    )
    args = parser.parse_args()

    inputfile = args.inputfile
    check_file(inputfile)

    outputfile = args.o
    if outputfile:
        check_file(outputfile, ext="csv")

    xlsx2csv(inputfile.name, outputfile=outputfile)
